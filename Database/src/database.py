import pickle
import os.path
import threading


class Database:
    def __init__(self, file_dir):
        self.file_dir = file_dir

        # If there is not a database file, we want to create it
        if not os.path.isfile(file_dir):
            self._state = {}
            with open(file_dir, 'wb') as file_obj:
                file_obj.write(pickle.dumps(self._state))

        else:
            with open(file_dir, 'rb') as file_obj:
                self._state = pickle.loads(file_obj.read())

    def read_whole_state(self):
        self.__read_state_from_file()
        return self._state

    def write_whole_state(self, state):
        self._state = state
        self.__write_state_to_file()

    def __read_state_from_file(self):
        with open(self.file_dir, 'rb') as file_obj:
            content = file_obj.read()

        self._state = pickle.loads(content)

    def __write_state_to_file(self):
        with open(self.file_dir, 'wb') as file_obj:
            file_obj.write(pickle.dumps(self._state))

    def __getitem__(self, item):
        # Read the file again
        self.__read_state_from_file()

        return self._state[item]

    def __setitem__(self, key, value):
        self._state[key] = value

        # Write the change to the file
        self.__write_state_to_file()


MAX_READING = 10


def _reading_decorator(func):
    def reading_and_call(*args, **kwargs):
        self = args[0]

        self.reading()
        to_return = func(*args, **kwargs)
        self.end_reading()

        return to_return

    return reading_and_call


def _writing_decorator(func):
    def writing_and_call(*args, **kwargs):
        self = args[0]

        self.writing()
        to_return = func(*args, **kwargs)
        self.end_writing()

        return to_return

    return writing_and_call


class SafeDatabase(Database):
    def __init__(self, file_dir):
        super().__init__(file_dir)
        self.reading_semaphore = threading.Semaphore(MAX_READING)
        self.writing_semaphore = threading.Semaphore()

    def reading(self, blocking=True):
        """
            This function notifies that the database is being reading by someone
        """
        self.reading_semaphore.acquire(blocking)

    def writing(self, blocking=True):
        """
            This function notifies that the database is being writing by someone
        """
        self.writing_semaphore.acquire(blocking)

        # TODO Check if this is how I should do it
        for i in range(MAX_READING):
            self.reading_semaphore.acquire(blocking)

    def end_reading(self):
        """
            This function notifies that someone has finished reading
        """
        self.reading_semaphore.release()

    def end_writing(self):
        """
            This function notifies that someone has finished writing
        """
        self.writing_semaphore.release()

        # TODO Check if this is how I should do it
        for i in range(10):
            self.reading_semaphore.release()

    # Redefining the I/O functions with decorators
    @_reading_decorator
    def read_whole_state(self, blocking=True):
        to_return = super().read_whole_state()
        return to_return

    @_writing_decorator
    def write_whole_state(self, state, blocking=True):
        super().write_whole_state(state)

    @_reading_decorator
    def __getitem__(self, item, blocking=True):
        to_return = super().__getitem__(item)
        return to_return

    @_writing_decorator
    def __setitem__(self, key, value, blocking=True):
        super().__setitem__(key, value)

    def can_write(self):
        """
            Checks if the next one has permissions to write
        """

        writing_unlocked = self.writing_semaphore.acquire(blocking=False)

        if not writing_unlocked:
            return False

        self.writing_semaphore.release()

        # Counter to know how many I need to release
        true_counter = 0
        for i in range(MAX_READING):
            reading_unlocked = self.reading_semaphore.acquire(blocking=False)
            if reading_unlocked:
                true_counter += 1
            else:
                writing_unlocked = False

        for i in range(true_counter):
            self.reading_semaphore.release()

        return writing_unlocked

    def can_read(self):
        """
            Checks if the next one has permissions to read
        """
        reading_unlocked = self.reading_semaphore.acquire(blocking=False)

        if reading_unlocked:
            self.reading_semaphore.release()

        return reading_unlocked

