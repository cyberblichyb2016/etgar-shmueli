from Database.src.database import Database, SafeDatabase
import pickle
import uuid


def generate_database_name():
    return "{}.db".format(str(uuid.uuid4()))


def generate_empty_database():
    return Database(generate_database_name())


def generate_random_dictionary(size, deep=1):
    my_dict = {}
    for i in range(size):
        my_dict[str(uuid.uuid4())] = str(uuid.uuid4()) if deep == 1 else generate_random_dictionary(size, deep-1)

    return my_dict


def generate_random_database(size, deep=1):
    my_dict = generate_random_dictionary(size, deep)
    database = generate_empty_database()
    database.write_whole_state(my_dict)

    return database


def generate_empty_safe_database():
    return SafeDatabase(generate_database_name())


def generate_random_safe_database(size, deep=1):
    my_dict = generate_random_dictionary(size, deep)
    database = generate_empty_safe_database()
    database.write_whole_state(my_dict)

    return database


class Simulators:
    @staticmethod
    def simulate_writing(safe_database):
        assert isinstance(safe_database, SafeDatabase)

        safe_database.writing(blocking=False)

    @staticmethod
    def simulate_reading(safe_database):
        assert isinstance(safe_database, SafeDatabase)

        safe_database.reading(blocking=False)

    @staticmethod
    def simulate_end_writing(safe_database):
        assert isinstance(safe_database, SafeDatabase)

        safe_database.end_writing()

    @staticmethod
    def simulate_end_reading(safe_database):
        assert isinstance(safe_database, SafeDatabase)

        safe_database.end_reading()


class TestDatabase:
    def test_create_new_database(self):
        database = generate_empty_database()

        assert database._state == {}

        with open(database.file_dir, 'rb') as file_obj:
            written_dict = pickle.loads(file_obj.read())
            assert written_dict == {}

    def test_read_from_database(self):
        database = generate_empty_database()

        assert database.read_whole_state() == {}

    def test_write_to_database(self):
        database = generate_empty_database()

        random_state = generate_random_dictionary(10, 3)
        database.write_whole_state(random_state)

        with open(database.file_dir, 'rb') as file_obj:
            written_dict = pickle.loads(file_obj.read())
            assert written_dict == random_state

        assert database.read_whole_state() == random_state

    def test_get_set_item(self):
        database = generate_empty_database()

        random_key = str(uuid.uuid4())
        random_value = str(uuid.uuid4())

        database[random_key] = random_value

        assert database[random_key] == random_value


class TestSafeDatabase:
    def test_no_traffic_reading(self):
        database = generate_empty_safe_database()

        state = database.read_whole_state()
        assert state == {}

    def test_no_traffic_writing(self):
        database = generate_empty_safe_database()

        random_state = generate_random_dictionary(10, 3)
        database.write_whole_state(random_state)

        assert database.read_whole_state() == random_state

    def test_no_traffic_get_set_item(self):
        database = generate_empty_safe_database()

        random_key = str(uuid.uuid4())
        random_value = str(uuid.uuid4())

        database[random_key] = random_value

        assert database[random_key] == random_value

    def test_cannot_read_while_someone_is_writing(self):
        database = generate_empty_safe_database()

        # Should can read in this stage
        assert database.can_read()

        Simulators.simulate_writing(database)

        # Should cannot read
        assert not database.can_read()

    def test_cannot_write_while_someone_is_reading(self):
        database = generate_empty_safe_database()

        # Should can write in this stage
        assert database.can_write()

        Simulators.simulate_reading(database)

        # Should cannot write
        assert not database.can_write()

    def test_many_can_read(self):
        database = generate_empty_safe_database()

        for i in range(10):
            assert database.can_read()
            Simulators.simulate_reading(database)

    def test_summary(self):
        database = generate_empty_safe_database()

        for i in range(10):
            assert database.can_read()
            Simulators.simulate_reading(database)

        assert not database.can_write()

        for i in range(10):
            Simulators.simulate_end_reading(database)

        assert database.can_write()
        Simulators.simulate_writing(database)

        assert not database.can_read()

        Simulators.simulate_end_writing(database)

        assert database.can_read()

